package com.pylypchuk.lifeimitation.controller;

import com.pylypchuk.lifeimitation.Constants;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Random;

@Slf4j
@Component
public class GameController {

    @Value("${window.width}")
    private double width;
    @Value("${window.height}")
    private double height;
    @Value("${cell.size}")
    private int cellSize;

    @FXML
    public Canvas canvas;
    @FXML
    public Button clearButton;
    @FXML
    public Button startStopButton;

    private GraphicsContext context;
    private boolean[] state;
    private int lineCount;
    private boolean startStop;

    @FXML
    public void initialize() {
        canvas.setWidth(width);
        canvas.setHeight(height);
        context = canvas.getGraphicsContext2D();

        state = new boolean[(int) width / cellSize * (int) height / cellSize];
        lineCount = (int) width / cellSize;

        canvas.addEventHandler(MouseEvent.MOUSE_CLICKED, (e) -> change(e.getX(), e.getY()));

        render();
    }

    @FXML
    private void startStop() {
        Platform.runLater(() -> {
            if (startStop) startStopButton.setText(Constants.STOP_LABEL);
            else startStopButton.setText(Constants.START_LABEL);
        });

        startStop = !startStop;
    }

    @FXML
    private void clear() {
        state = new boolean[(int) width / cellSize * (int) height / cellSize];
        render();
    }

    @FXML
    private void close() {
        Platform.exit();
    }

    private void change(double x, double y) {
        Platform.runLater(() -> {
            int yIndex = (int) y / cellSize;
            int xIndex = (int) x / cellSize;

            int index = yIndex * lineCount + xIndex;

            state[index] = !state[index];

            render();
        });
    }

    private void render() {
        context.setFill(Color.BLACK);
        context.fillRect(0, 0, width, height);

        context.setFill(Color.GREENYELLOW);
        for (int i = 0; i < state.length; i++) {
            if (state[i]) {
                int xFill = i % lineCount * cellSize;
                int yFill = i / lineCount * cellSize;
                context.fillRect(xFill, yFill, cellSize, cellSize);
            }
        }
    }

    @Scheduled(fixedDelay = 100)
    private void update() {
        if (startStop) {

            boolean[] newState = state.clone();

            for (int i = 0; i < state.length; i++) {
                if (state[i]) {
                    newState[i] = handleActiveCell(i);
                } else {
                    newState[i] = handleInactiveCell(i);
                }
            }

            if (Arrays.equals(state, newState)) {
                startStop();
            }

            state = newState;
            render();
        }
    }

    private boolean handleInactiveCell(int index) {
        int activeNeighborsCount = getActiveNeighborsCount(index);
        return activeNeighborsCount == 3;
    }

    private boolean handleActiveCell(int index) {
        int activeNeighborsCount = getActiveNeighborsCount(index);
        return activeNeighborsCount == 2 || activeNeighborsCount == 3;
    }

    private int getActiveNeighborsCount(int index) {
        int activeNeighborsCount = 0;

        activeNeighborsCount += incrementIfActive(index - lineCount);
        activeNeighborsCount += incrementIfActive(index + lineCount);

        boolean isLeft = index % lineCount == 0;
        if (isLeft) {
            activeNeighborsCount += incrementIfActive(index + 1);
            activeNeighborsCount += incrementIfActive(index - lineCount + 1);
            activeNeighborsCount += incrementIfActive(index + lineCount + 1);
            return activeNeighborsCount;
        }
        boolean isRight = index % lineCount + 1 == lineCount;
        if (isRight) {
            activeNeighborsCount += incrementIfActive(index - 1);
            activeNeighborsCount += incrementIfActive(index - lineCount - 1);
            activeNeighborsCount += incrementIfActive(index + lineCount - 1);
            return activeNeighborsCount;
        }

        activeNeighborsCount += incrementIfActive(index - 1);
        activeNeighborsCount += incrementIfActive(index + 1);
        activeNeighborsCount += incrementIfActive(index - lineCount - 1);
        activeNeighborsCount += incrementIfActive(index - lineCount + 1);
        activeNeighborsCount += incrementIfActive(index + lineCount - 1);
        activeNeighborsCount += incrementIfActive(index + lineCount + 1);

        return activeNeighborsCount;
    }

    private int incrementIfActive(int index) {
        try {
            return state[index] ? 1 : 0;
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }

    @FXML
    private void random() {
        Random random = new Random();

        boolean[] newState = new boolean[state.length];
        for (int i = 0; i < newState.length; i++) {
            newState[i] = random.nextBoolean();
        }

        state = newState;

        render();
    }
}
