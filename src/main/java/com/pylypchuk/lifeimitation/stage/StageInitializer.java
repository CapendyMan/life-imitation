package com.pylypchuk.lifeimitation.stage;

import com.pylypchuk.lifeimitation.stage.event.StageReadyEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Component
public class StageInitializer implements ApplicationListener<StageReadyEvent> {

    private final Resource resource;
    private final String title;
    private final double width;
    private final double height;
    private final ApplicationContext applicationContext;

    public StageInitializer(ApplicationContext applicationContext,
                            @Value("classpath:/main.fxml") Resource resource,
                            @Value("${application.title}") String title,
                            @Value("${window.width}") double width,
                            @Value("${window.height}") double height) {
        this.applicationContext = applicationContext;
        this.resource = resource;
        this.title = title;
        this.width = width;
        this.height = height;
    }

    @SneakyThrows
    @Override
    public void onApplicationEvent(StageReadyEvent stageReadyEvent) {
        FXMLLoader loader = new FXMLLoader(resource.getURL());
        loader.setControllerFactory(applicationContext::getBean);

        Parent parent = loader.load();
        Stage stage = stageReadyEvent.getStage();
        stage.setScene(new Scene(parent, width, height + 104));
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setTitle(title);
        stage.show();
    }
}
