package com.pylypchuk.lifeimitation;

import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class LifeImitationApplication {

    public static void main(String[] args) {
        Application.launch(GameApplication.class, args);
    }
}
