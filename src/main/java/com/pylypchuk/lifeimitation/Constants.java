package com.pylypchuk.lifeimitation;

public class Constants {
    public static final String STOP_LABEL = "STOP";
    public static final String START_LABEL = "START";
}
